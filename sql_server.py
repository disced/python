
import pyodbc
import re
'''
Docu
    https://github.com/mkleehammer/pyodbc/wiki
    https://github.com/mkleehammer/pyodbc/wiki/Cursor
'''
class SQLServer:
    
    # ----------------------|    Private Attributes   |---------------------- 
    __has_conn_string   = False
    __has_connected     = False
    __connection        = None
    __connection_string = None
    __cursor            = None
    
    def __init__(self, 
                 driver   = None,
                 server   = None,
                 database = None,
                 uid      = None,
                 pwd      = None):
        self.__driver   = driver
        self.__server   = server
        self.__database = database
        self.__uid      = uid
        self.__pwd      = pwd
        
        self.__create_connection_string()

    def set_driver(self, driver):
        self.__driver = driver
    def set_server(self, server):
        self.__server = server
    def set_database(self, database):
        self.__database = database
    def set_uid(self, uid):
        self.__uid = uid
    def set_pwd(self, pwd):
        self.__pwd = pwd
        
    
    def connect_2_db(self):
        if self.__has_conn_string:
            try:
                self.__connection = pyodbc.connect(self.__connection_string)
                self.__create_cursor()
                self.__has_connected = True
            except:
                self.__has_connected = False
        else: raise Exception('Define correct connection string') # <- hace falta?

    def query(self, query: str, columns: list, table: str):

        formated_query = self.__format_query(query, columns, table)
        print(formated_query)            
        
        if self.__has_connected:
            return self.__cursor.execute(formated_query)
        
        
    
    # ----------------------|    Private methods   |---------------------- 
    def __create_connection_string(self):
        if  len(self.__driver)   and \
            len(self.__server)   and \
            len(self.__database) and \
            len(self.__uid)      and \
            len(self.__pwd):
            self.__connection_string = \
                (\
                "Driver={"   + self.__driver   + \
                "};Server="  + self.__server   + \
                ";Database=" + self.__database + \
                ";UID="      + self.__uid      + \
                ";PWD="       + self.__pwd      + ";"
                )
            self.__has_conn_string = True
        
        else: raise Exception('Define all data for connection string')

    
    def __create_cursor(self):
        self.__cursor = self.__connection.cursor()
        
    def __format_query(self, query: str, columns: list, table: str):
        cols = ''
        
        for col in columns:
            cols += col + ', '

        # Invierte el str [::-1]
        # Selecciona el str desde la posición 1 al final [1:]
        # Le da la vuelta [::-1]
        cols = re.split('[,]', cols[::-1], 1)[1][::-1] 
       
        new_query = query.format(cols, table)
        new_query = re.split('[;]', new_query, 1)[0]        
        return new_query
    
